//Код не полностью мой, в университете мне выдали каркас, основные части полностью мои
#include "align.h"
#include <string>

using std::string;
using std::cout;
using std::endl;

using std::tuple;
using std::get;
using std::tie;
using std::make_tuple;
using std::sort;

struct shp {
    int metr=0;
    int vert=0;
    int hor=0;
};

shp optshift(Image img1, Image img2, uint height, uint width) {     //поиск сдвига
    shp min, max;
    int MSE=0/*, KOR=0*/;

    min.metr=256*256;       //maximum value possible
    max.metr=0;     //minimum value possible
    for (int n=-25; n<=25; n++) {       //смещение по столбцам
        for (int m=-25; m<=25; m++) {       //смещение по строкам
            if (n<0) {      //если смещение по столбцам меньше 0 (up)
                if (m<0) {      //если смещение по строкам меньше 0 (left)
                    for (uint i=0; i<=height/3+n-1; i++) {      //вверх и влево
                        for (uint j=0; j<=width+m-1; j++) {
                            //среднеквадратичное отклонение
                            MSE+=( ( get<2>(img1(i, j)) ) - ( get<1>(img2(i-n, j-m)) ) ) * ( ( get<2>(img1(i, j)) ) - ( get<1>(img2(i-n, j-m)) ) );
                            //кросс-корреляция
                            //KOR+=(get<2>(img1(i, j)))*(get<1>(img2(i-n, j-m)));
                        }
                    }
                    MSE=MSE/(height*width);
                    if (MSE<min.metr) {     //среднеквадратичное отклонение
                        min.metr=MSE;
                        min.vert=n;
                        min.hor=m;
                    }
                    /*if (KOR>max.metr) {     //кросс-корреляция
                        max.metr=KOR;
                        max.vert=n;
                        max.hor=m;
                    }*/
                } else {        //вверх и впрпаво
                    for (uint i=0; i<=height/3+n-1; i++) {
                        for (uint j=m; j<=width-1; j++) {
                            //среднеквадратичное отклонение
                            MSE+=( ( get<2>(img1(i, j)) ) - ( get<1>(img2(i-n, j-m)) ) ) * ( ( get<2>(img1(i, j)) ) - ( get<1>(img2(i-n, j-m)) ) );
                            //кросс-корреляция
                            //KOR+=(get<2>(img1(i, j)))*(get<1>(img2(i-n, j-m)));
                        }
                    }
                    MSE=MSE/(height*width);
                    if (MSE<min.metr) {     //среднеквадратичное отклонение
                        min.metr=MSE;
                        min.vert=n;
                        min.hor=m;
                    }
                    /*if (KOR>max.metr) {     //кросс-корреляция
                        max.metr=KOR;
                        max.vert=n;
                        max.hor=m;
                    }*/
                };
            } else {
                if (m<0) {      //down and left
                    for (uint i=n; i<=height/3-1; i++) {
                        for (uint j=0; j<=width+m-1; j++) {
                            //среднеквадратичное отклонение
                            MSE+=( ( get<2>(img1(i, j)) ) - ( get<1>(img2(i-n, j-m)) ) ) * ( ( get<2>(img1(i, j)) ) - ( get<1>(img2(i-n, j-m)) ) );
                            //кросс-корреляция
                            //KOR+=(get<2>(img1(i, j)))*(get<1>(img2(i-n, j-m)));
                        }
                    }
                    MSE=MSE/(height*width);
                    if (MSE<min.metr) {     //среднеквадратичное отклонение
                        min.metr=MSE;
                        min.vert=n;
                        min.hor=m;
                    }
                    /*if (KOR>max.metr) {     //кросс-корреляция
                        max.metr=KOR;
                        max.vert=n;
                        max.hor=m;
                    }*/
                } else {        //down and right
                    for (uint i=n; i<=height/3-1; i++) {
                        for (uint j=m; j<=width-1; j++) {
                            //среднеквадратичное отклонение
                            MSE+=( ( get<2>(img1(i, j)) ) - ( get<1>(img2(i-n, j-m)) ) ) * ( ( get<2>(img1(i, j)) ) - ( get<1>(img2(i-n, j-m)) ) );
                            //кросс-корреляция
                            //KOR+=(get<2>(img1(i, j)))*(get<1>(img2(i-n, j-m)));
                        }
                    }
                    MSE=MSE/(height*width);
                    if (MSE<min.metr) {     //среднеквадратичное отклонение
                        min.metr=MSE;
                        min.vert=n;
                        min.hor=m;
                    }
                    /*if (KOR>max.metr) {     //кросс-корреляция
                        max.metr=KOR;
                        max.vert=n;
                        max.hor=m;
                    }*/
                }
            };
        }
    }

    return min;
    //return max;       //KOR
}

Image shift(Image img, int vert, int hor) {     //сдвиг
    long int height=img.n_rows, width=img.n_cols;

    if (vert<0) {
        for (long int i=0; i<height; i++) {      //вертикальный сдвиг влево
            for (long int j=0; j<width; j++) {
                if (i-vert<height) {
                    img(i, j)=img(i-vert, j);
                } else {
                    img(i, j)=make_tuple(0, 0, 0);
                };
            }
        }
    } else if (vert>0) {        //вертикальный сдвиг вправо
        for (long int i=height-1; i>=0; i--) {
            for (long int j=width-1; j>=0; j--) {
                if (i-vert>=0) {
                    img(i, j)=img(i-vert, j);
                } else {
                    img(i, j)=make_tuple(0, 0, 0);
                };
            }
        }
    };
    if (hor<0) {        //горизрнтальный сдвиг вверх
        for (long int i=0; i<height; i++) {
            for (long int j=0; j<width; j++) {
                if (j-hor<width) {
                    img(i, j)=img(i, j-hor);
                } else {
                    img(i, j)=make_tuple(0, 0, 0);
                };
            }
        }
    } else if (hor>0) {     //горизонтальный сдвиг вниз
        for (long int i=height-1; i>=0; i--) {
            for (long int j=width-1; j>=0; j--) {
                if (j-hor>=0) {
                    img(i, j)=img(i, j-hor);
                } else {
                    img(i, j)=make_tuple(0, 0, 0);
                };
            }
        }
    };
    return img;
}

Image align(Image srcImage, bool isPostprocessing, std::string postprocessingType, double fraction, bool isMirror, 
            bool isInterp, bool isSubpixel, double subScale)
{
        Image simg, bimg, gimg, rimg;
    uint height=srcImage.n_rows, width=srcImage.n_cols;
    //uint r, g, b;
    shp min/*, max*/;

    bimg=srcImage.submatrix(0, 0, height/3, width);                 //разбиваю изображение на 3
    gimg=srcImage.submatrix(height/3, 0, height/3, width);
    rimg=srcImage.submatrix(2*height/3, 0, height/3, width);

    bimg=resize(bimg, 2);       //поиск сдвига с субпиксельной точностью
    gimg=resize(gimg, 2);
    rimg=resize(rimg, 2);
    simg=bimg;

    //max=optshift(bimg, gimg, height, width);        //KOR
    //gimg=shift(gimg, max.vert, max.hor);

    //max=optshift(gimg, rimg, height, width);      //KOR
    //rimg=shift(rimg, max.vert, max.hor);

    //max=optshift(gimg, bimg, height, width);      //KOR
    //bimg=shift(bimg, max.vert, max.hor);

    min=optshift(rimg, bimg, height, width);
    bimg=shift(bimg, min.vert, min.hor);

    min=optshift(rimg, gimg, height, width);
    gimg=shift(gimg, min.vert, min.hor);


    for (uint i=0; i<simg.n_rows; i++) {     //совмещение изображений
        for (uint j=0; j<simg.n_cols; j++) {
            simg(i, j)=make_tuple(get<0>(rimg(i, j)), get<1>(gimg(i, j)), get<2>(bimg(i, j)));
        }
    }

    simg=resize(simg, 0.5);

    srcImage=simg;
    return srcImage;
}

Image sobel_x(Image src_image) {
    Matrix<double> kernel = {{-1, 0, 1},
                             {-2, 0, 2},
                             {-1, 0, 1}};
    return custom(src_image, kernel);
}

Image sobel_y(Image src_image) {
    Matrix<double> kernel = {{ 1,  2,  1},
                             { 0,  0,  0},
                             {-1, -2, -1}};
    return custom(src_image, kernel);
}

Image unsharp(Image src_image) {
    double one=1, three=3;
    double a=-(one/6), b=-(2/three), c=13/three;
    Matrix<double> kernel= {{a, b, a},
                            {b, c, b},
                            {a, b, a}};
    
    src_image=custom(src_image, kernel);
    return src_image;
}

Image gray_world(Image src_image) {
    double Sr=0, Sg=0, Sb=0, S=0;
    uint height=src_image.n_rows, width=src_image.n_cols, r, g, b;

    for (uint i=0; i<height; i++) {
        for (uint j=0; j<width; j++) {
            tie(r, g, b)=src_image(i, j);
            Sr+=r;
            Sg+=g;
            Sb+=b;
        }
    }

    Sr/=(width*height);
    Sg/=(width*height);
    Sb/=(width*height);
    S=(Sr+Sg+Sb)/3;

    for (uint i=0; i<height; i++) {
        for (uint j=0; j<width; j++) {
            tie(r, g, b)=src_image(i, j);
            r*=S/Sr;
            g*=S/Sg;
            b*=S/Sb;
            src_image(i, j)=make_tuple(r, g, b);
        }
    }

    return src_image;
}

Image resize(Image src_image, double scale) {
    double S;
    Image img(scale*src_image.n_rows, scale*src_image.n_cols);

     if (scale<0.0) {
        std::cout<<"scale value <0\n";
        return src_image;
    } else {
        for (uint i=0; i<src_image.n_rows; i++) {
            for (uint j=0; j<src_image.n_cols; j++) {
                S=scale;
                while (S/1>0.0) {
                    img(scale*i, scale*j+(S/1-1))=src_image(i, j);
                    img(scale*i+(S/1-1), scale*j)=src_image(i, j);
                    img(scale*i+(S/1-1), scale*j+(S/1-1))=src_image(i, j);
                    S--;
                }
            }
        }
    }

    src_image=img;

    return src_image;
}

Image custom(Image src_image, Matrix<double> kernel) {
    // Function custom is useful for making concrete linear filtrations
    // like gaussian or sobel. So, we assume that you implement custom
    // and then implement other filtrations using this function.
    // sobel_x and sobel_y are given as an example.
    uint height, width, r, g, b;
    double rsum=0, gsum=0, bsum=0;
    uint rad=(kernel.n_cols-1)/2, rs, gs, bs;
    Image img(src_image.n_rows+rad*2, src_image.n_cols+rad*2);

    width=src_image.n_cols;
    height=src_image.n_rows;

    for (uint i=0; i<img.n_rows; i++) {     //увеличение изображения на радиус ядра с каждого края
        for (uint j=0; j<img.n_cols; j++) {
            /*if (i<rad || i>=img.n_rows-rad || j<rad || j>=img.n_cols-rad) {       //дополнение изображения черными краями
                img(i, j)=make_tuple(0, 0, 0);
            } else {
                img(i, j)=src_image(i-rad, j-rad);
            };*/

            if (i<rad && j<rad) {       //зеркальное дополнение изображения
                img(i, j)=src_image(rad-i, rad-j);
            } else if (i>=rad && i<img.n_rows-rad && j<rad) {
                img(i, j)=src_image(i-rad, rad-j);
            } else if (i>=img.n_rows-rad && j<rad) {
                img(i, j)=src_image(2*(img.n_rows-rad)-i-rad-1, rad-j);
            } else if (i<rad && j>=rad && j<img.n_cols-rad) {
                img(i, j)=src_image(rad-i, j-rad);
            } else if (i<rad && j>=img.n_cols-rad) {
                img(i, j)=src_image(rad-i, 2*(img.n_cols-rad)-j-rad-1);      
            } else if (i>=rad && i<img.n_rows-rad && j>=img.n_cols-rad) {
                img(i, j)=src_image(i-rad, 2*(img.n_cols-rad)-j-rad-1);
            } else if (i>=img.n_rows-rad && j>=img.n_cols-rad) {
                img(i, j)=src_image(2*(img.n_rows-rad)-i-rad-1, 2*(img.n_cols-rad)-j-rad-1);
            } else if (i>=img.n_rows-rad && j>=rad && j<img.n_cols-rad) {
                img(i, j)=src_image(2*(img.n_rows-rad)-i-rad-1, j-rad);  
            } else {
                img(i, j)=src_image(i-rad, j-rad);
            }
        }
    }

    for (uint i=rad; i<height+rad; i++) {
        for (uint j=rad; j<width+rad; j++) {
            for (uint n=0; n<kernel.n_rows; n++) {
                for (uint m=0; m<kernel.n_cols; m++) {    
                    tie(r, g, b)=img(i-rad+n, j-rad+m);
                    rsum+=r*kernel(n, m);
                    gsum+=g*kernel(n, m);
                    bsum+=b*kernel(n, m);
                }
            }
            if (rsum<0) rsum=0;
            if (rsum>255) rsum=255;
            if (gsum<0) gsum=0;
            if (gsum>255) gsum=255;
            if (bsum<0) bsum=0;
            if (bsum>255) bsum=255;
            rs=rsum;
            gs=gsum;
            bs=bsum;

            src_image(i-rad, j-rad)=make_tuple(rs, gs, bs);

            rsum=0;
            gsum=0;
            bsum=0;
        }
    }

    return src_image;
    //return img;
}

Image autocontrast(Image src_image, double fraction) {
    uint Gist[256]={0};
    uint min=(fraction*src_image.n_rows*src_image.n_cols)/2;
    uint Y, r, g, b, ymin=0, ymax=255;
    double sum=0, k, l;

    for (uint i=0; i<src_image.n_rows; i++) {
        for (uint j=0; j<src_image.n_cols; j++) {
            tie(r, g, b)=src_image(i, j);
            Y=0.2125*r+0.7154*g+0.0721*b;
            Y/=1;
            Gist[Y]++;
        }
    }

    while (sum<=min) {
        sum+=Gist[ymin];
        ymin++;
    }

    sum=0;

    while (sum<=min) {
        sum+=Gist[ymax];
        ymax--;
    }

    k=255.0/(ymax-ymin);
    l=255.0*ymin/(ymax-ymin);

    for (uint i=0; i<src_image.n_rows; i++) {
        for (uint j=0; j<src_image.n_cols; j++) {
            tie(r, g, b)=src_image(i, j);
            r=k*r+l;
            g=k*g+l;
            b=k*b+l;
            if (r>255) r=255;
            if (g>255) g=255;
            if (b>255) b=255;
            src_image(i, j)=make_tuple(r, g, b);
        }
    }

    return src_image;
}

Image gaussian(Image src_image, double sigma, int radius)  {
    Matrix<double> Gkernel (radius*2+1, radius*2+1);
    double G, pi=3.14159;
    double ker_i, ker_j, Gsum=0, Gs=0;

    for (int i=0; i<radius*2+1; i++) {
        for (int j=0; j<radius*2+1; j++) {
            ker_i=(i-radius)*(i-radius);
            ker_j=(j-radius)*(j-radius);

            G=exp(-((ker_i+ker_j)/2*sigma*sigma));
            G=G/(2*pi*sigma*sigma);
            Gkernel(i, j)=G;
            Gsum+=G;//
        }
    }

    Gsum=1/Gsum;//

    for (int i=0; i<radius*2+1; i++) {      //normalization??????????
        for (int j=0; j<radius*2+1; j++) {
            Gkernel(i, j)*=Gsum;
            Gs+=Gkernel(i, j);
        }
    }

    src_image=custom(src_image, Gkernel);//

    return src_image;
}

Image gaussian_separable(Image src_image, double sigma, int radius) {
    return src_image;
}

Image median(Image src_image, int radius) {
    uint R[(2*radius+1)*(2*radius+1)];
    uint G[(2*radius+1)*(2*radius+1)];
    uint B[(2*radius+1)*(2*radius+1)];
    uint r, g, b, k=0;
    uint size=2*radius+1, rad=radius;
    Image img(src_image.n_rows+radius*2, src_image.n_cols+radius*2);

    for (uint i=0; i<img.n_rows; i++) {
        for (uint j=0; j<img.n_cols; j++) {
            if (i<rad || i>=img.n_rows-radius || j<rad || j>=img.n_cols-radius) {       //дополнение изображения черными краями
                img(i, j)=make_tuple(0, 0, 0);
            } else {
                img(i, j)=src_image(i-radius, j-radius);
            };
        }
    }

    for (uint i=radius; i<src_image.n_rows+radius; i++) {
        for (uint j=radius; j<src_image.n_cols+radius; j++) {
            k=0;
            for (uint n=0; n<size; n++) {
                for (uint m=0; m<size; m++) {
                    tie(r, g, b)=img(i-radius+n, j-radius+m);
                    R[k]=r;
                    G[k]=g;
                    B[k]=b;
                    k++;
                }
            }
            sort(R, R+size*size);
            sort(G, G+size*size);
            sort(B, B+size*size);
            k=size*size/2;
            src_image(i-radius, j-radius)=make_tuple(R[k], G[k], B[k]);
        }
    }

    return src_image;
}

Image median_linear(Image src_image, int radius) {
    return src_image;
}

Image median_const(Image src_image, int radius) {
    return src_image;
}

Image canny(Image src_image, int threshold1, int threshold2) {
    return src_image;
}
